import numpy as np
from .model import Assignment, AssignmentProblem, NormalizedAssignmentProblem
from typing import List, Dict, Tuple, Set
from numpy.typing import ArrayLike

class Solver:
    '''
    A hungarian solver for the assignment problem.

    Methods:
    --------
    __init__(problem: AssignmentProblem):
        creates a solver instance for a specific problem
    solve() -> Assignment:
        solves the given assignment problem
    extract_mins(costs: ArrayLike):
        substracts from columns and rows in the matrix to create 0s in the matrix
    find_max_assignment(costs: ArrayLike) -> Dict[int,int]:
        finds the biggest possible assinments given 0s in the cost matrix
        result is a dictionary, where index is a worker index, value is the task index
    add_zero_by_crossing_out(costs: ArrayLike, partial_assignment: Dict[int,int])
        creates another zero(s) in the cost matrix by crossing out lines (rows/cols) with zeros in the cost matrix,
        then substracting/adding the smallest not crossed out value
    create_assignment(raw_assignment: Dict[int, int]) -> Assignment:
        creates an assignment instance based on the given dictionary assignment
    '''
    def __init__(self, problem: AssignmentProblem):
        self.problem = NormalizedAssignmentProblem.from_problem(problem)

    def solve(self) -> Assignment:
        costs = np.array(self.problem.costs)

        while True:
            self.extracts_mins(costs)
            max_assignment = self.find_max_assignment(costs)
            if len(max_assignment) == self.problem.size():
                return self.create_assignment(max_assignment)
            self.add_zero_by_crossing_out(costs, max_assignment)

    def extracts_mins(self, costs: ArrayLike):
        #TODO: substract minimal values from each row and column
        
        for i in range(self.problem.size()): 
            actual = []             # Odejmuje najmniejsza wartosc w kazdym wierszu
            for j in range(self.problem.size()):
                actual.append(costs[i][j])
            minim1 = min(actual)
            for x in range(self.problem.size()):
                costs[i][x] -= minim1
                
        for i in range(self.problem.size()):
            actual = []  # Odejmuje najmniejsza wartosc w kazdej kolumnie
            for j in range(self.problem.size()):
                actual.append(costs[j][i])
            minim1 = min(actual)
            for x in range(self.problem.size()):
                costs[x][i] -= minim1
            # print(self.problem.costs)
        pass

    def add_zero_by_crossing_out(self, costs: ArrayLike, partial_assignment: Dict[int,int]):
        #TODO:
        # 1) "mark" columns and rows according to the instructions given by teacher
        # 2) cross out marked columns and not marked rows
        # 3) find minimal uncrossed value and subtract it from the cost matrix
        # 4) add the same value to all crossed out columns and rows

        wierszeZpoziomaStrzalka = []
        kolumnyZpionowaStrzalka = []
        for i in range(self.problem.size()):                          # krok 1
            if i not in partial_assignment.keys():
                if i not in wierszeZpoziomaStrzalka:
                    wierszeZpoziomaStrzalka.append(i)
        zmiana = True
        while zmiana:                                                # zapetlone kroki 2 i 3 dopoki sa zmiany
            zmiana = False
            for i in wierszeZpoziomaStrzalka:                        # krok 2
                for j in range(self.problem.size()):
                    if costs[i][j] == 0:
                        if j not in kolumnyZpionowaStrzalka:
                            kolumnyZpionowaStrzalka.append(j)
                            zmiana = True
            for wiersz, kolumna in partial_assignment.items():       # krok 3
                if kolumna in kolumnyZpionowaStrzalka:
                    if wiersz not in wierszeZpoziomaStrzalka:
                        wierszeZpoziomaStrzalka.append(wiersz)
                        zmiana = True
        wykresloneWiersze = []
        wykresloneKolumny = []
        for i in range(self.problem.size()):                         # wykreslanie odpowiednich kolumn i wierszy
            if i not in wierszeZpoziomaStrzalka:
                wykresloneWiersze.append(i)
            if i in kolumnyZpionowaStrzalka:
                wykresloneKolumny.append(i)
        potencjalneMin = []
        for i in range(self.problem.size()):                         # szukanie min z niewykreslonej czesci tabeli
            for j in range(self.problem.size()):
                if i not in wykresloneWiersze and j not in wykresloneKolumny:
                    potencjalneMin.append(costs[i][j])
        minZnieWykreslonej = min(potencjalneMin)
        for i in range(self.problem.size()):
            for j in range(self.problem.size()):
                if i not in wykresloneWiersze and j not in wykresloneKolumny:        # odejmowanie min z niewykreslonych komorek
                    costs[i][j] -= minZnieWykreslonej
                if i in wykresloneWiersze and j in wykresloneKolumny:                # dodawanie min do 2-krotnie wykreslonych komorek
                    costs[i][j] += minZnieWykreslonej
        pass

    def find_max_assignment(self, costs) -> Dict[int,int]:
        partial_assignment = dict()
         #TODO: find the biggest assignment in the cost matrix
        # 1) always try first the row with the least amount of 0s
        # 2) then use column with the least amount of 0s
        # TIP: remember, rows and cols can't repeat in the assignment
        #      partial_assignment[1] = 2 means that the worker with index 1
        #                                has been assigned to task with index 2
        rows = []
        cols = []
        rowsCounter = 0
        colsCounter = 0
        for i in range(self.problem.size()):
            row = []
            col = []
            for j in range(self.problem.size()):
                if costs[i][j] == 0:
                    row.append(j)
                    rowsCounter += 1
                if costs[j][i] == 0:
                    col.append(j)
                    colsCounter += 1
            rows.append(row)
            cols.append(col)

        while rowsCounter > 0 and colsCounter > 0:
            #choose row
            chosenRow = -1
            zeros = self.problem.size() + 1
            for i in range(len(rows)):
                if len(rows[i]) < zeros and len(rows[i]) > 0 and i not in partial_assignment.keys():
                    zeros = len(rows[i])
                    chosenRow = i

            #chosse col
            if chosenRow == -1:
                return partial_assignment

            chosenCol = -1
            zeros = self.problem.size() + 1
            for i in rows[chosenRow]:
                if len(cols[i]) < zeros and len(cols[i]) > 0:
                    zeros = len(cols[i])
                    chosenCol = i

            #add chosen pair to Dict
            if chosenCol >= 0 and chosenRow >= 0:
                partial_assignment.update({chosenRow: chosenCol})
            else:
                return partial_assignment

            #delete
            for i in range(len(rows)):
                for j in rows[i]:
                    if j == chosenCol:
                        rows[i].remove(j)
                        rowsCounter -= 1
                        break

            for i in range(len(cols)):
                for j in cols[i]:
                    if j == chosenRow:
                        cols[i].remove(j)
                        colsCounter -= 1
                        break

        return partial_assignment

    def create_assignment(self, raw_assignment: Dict[int,int]) -> Assignment:
        #TODO: create an assignment instance based on the dictionary
        # tips:
        # 1) use self.problem.original_problem.costs to calculate the cost
        # 2) in case the original cost matrix (self.problem.original_problem.costs wasn't square)
        #    and there is more workers than task, you should assign -1 to workers with no task

        assignment = []
        total_cost = 0
        #for all workers
        for i in range(self.problem.size()):
            #if it is more workers than tasks:
            if i < len(self.problem.original_problem.costs):
                if raw_assignment[i] < len(self.problem.original_problem.costs[0]):
                    assignment.append(raw_assignment[i])
                    total_cost += self.problem.original_problem.costs[i][raw_assignment[i]]
                else:
                    assignment.append(-1)
            else:
                assignment.append(-1)
                
        return Assignment(assignment, total_cost)
