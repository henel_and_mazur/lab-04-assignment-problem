import numpy as np
from .model import AssignmentProblem, Assignment, NormalizedAssignmentProblem
from ..simplex.model import Model
from ..simplex.expressions.expression import Expression
from dataclasses import dataclass
from typing import List 



class Solver:
    '''
    A simplex solver for the assignment problem.

    Methods:
    --------
    __init__(problem: AssignmentProblem):
        creates a solver instance for a specific problem
    solve() -> Assignment:
        solves the given assignment problem
    '''
    def __init__(self, problem: AssignmentProblem):
        self.problem = NormalizedAssignmentProblem.from_problem(problem)


    def solve(self) -> Assignment:
        model = Model("assignment")
        # TODO:
        # 1) creates variables, one for each cost in the cost matrix
        # 4) add constraint, that every variable has to be <= 1
        # 5) create an objective expression, involving all variables weighted by their cost
        # 6) add the objective to model (minimize it!)
        objExpr = None
        for i in range(self.problem.size()):
            for j in range(self.problem.size()):
                x = model.create_variable(f'x{i}{j}')
                model.add_constraint(1.0 * model.variables[i * self.problem.size() + j] <= 1)
                if objExpr is None:
                    if self.problem.original_problem.objective_is_min:
                        objExpr = self.problem.costs[i][j] * x
                    else:
                        if i < len(self.problem.original_problem.costs) and j < len(self.problem.original_problem.costs[0]):
                            objExpr = (self.problem.original_problem.costs[i][j]) * x
                        else:
                            objExpr = 0.0 * x
                else:
                    if self.problem.original_problem.objective_is_min:
                        objExpr = objExpr + self.problem.costs[i][j] * x
                    else:
                        if i < len(self.problem.original_problem.costs) and j < len(
                                self.problem.original_problem.costs[0]):
                            objExpr = objExpr + (self.problem.original_problem.costs[i][j]) * x
                        else:
                            objExpr = objExpr + 0.0 * x
        if self.problem.original_problem.objective_is_min:
            model.minimize(objExpr)
        else:
            model.maximize(objExpr)
        # 2) add constraint, that sum of every row has to be equal 1
        # 3) add constraint, that sum of every col has to be equal 1
        for i in range(self.problem.size()):
            row = None
            col = None
            for j in range(self.problem.size()):
                if row is None:
                    row = 1.0 * model.variables[i * self.problem.size() + j]
                else:
                    row = row + 1.0 * model.variables[i * self.problem.size() + j]
                if col is None:
                    col = 1.0 * model.variables[j * self.problem.size() + i]
                else:
                    col = col + 1.0 * model.variables[j * self.problem.size() + i]
            model.add_constraint(row == 1)
            model.add_constraint(col == 1)

        solution = model.solve()
        # TODO:
        # 1) extract assignment for the original problem from the solution object
        # tips:
        # - remember that in the original problem n_workers() not alwyas equals n_tasks()
        assigned_tasks = []
        org_objective = 0
        for i in range(len(self.problem.original_problem.costs)):
            assigned_tasks.append(-1)
            for j in range(self.problem.size()):
                if solution.assignment(model)[i * self.problem.size() + j] == 1 and j < len(self.problem.original_problem.costs[0]):
                    assigned_tasks[i] = j
                    org_objective += self.problem.costs[i][j]
                    break
        org_objective = solution.objective_value()
        return Assignment(assigned_tasks, org_objective)




